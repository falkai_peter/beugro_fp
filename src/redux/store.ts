import { createStore, applyMiddleware } from "redux";
import asyncMiddleware from "@redux/asyncMiddleware"
import reducer from "@redux/reducer"
import ICovidData from "../types/ICovidData";

interface IStore {
  latest: ICovidData
  history: ICovidData[]
  
}

export const initStore: IStore = {
  latest: {
    infected: 0,
    recovered: 0,
    activeInfected: 0,
    lastUpdatedAtApify: ''
  },
  history:[]
};

export type Store = typeof initStore;

export const store = createStore(reducer, applyMiddleware(asyncMiddleware));
