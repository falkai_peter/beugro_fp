import ICovidData from "src/types/ICovidData";
import { FC } from "react";
import { PieChart, Pie, ResponsiveContainer } from "recharts";
import chartColors from "@components/chartColors";

type Props = {
  height: number;
  data: ICovidData[];
};

const CustomPieChart: FC<Props> = props => {
  return (
    <ResponsiveContainer height={props.height} width="100%">
      <PieChart>
        <Pie
          data={props.data.map(({ infected, lastUpdatedAtApify }) => {
            return { infected, lastUpdatedAtApify };
          })}
          dataKey="infected"
          nameKey="lastUpdatedAtApify"
          cx="50%"
          cy="50%"
          outerRadius={50}
          fill={chartColors.infected}
        />
        <Pie
          data={props.data.map(({ recovered, lastUpdatedAtApify }) => {
            return { recovered, lastUpdatedAtApify };
          })}
          dataKey="recovered"
          nameKey="lastUpdatedAtApify"
          cx="50%"
          cy="50%"
          innerRadius={50}
          outerRadius={80}
          fill={chartColors.recovered}
        />
        <Pie
          data={props.data.map(({ activeInfected, lastUpdatedAtApify }) => {
            return { activeInfected, lastUpdatedAtApify };
          })}
          dataKey="activeInfected"
          nameKey="lastUpdatedAtApify"
          cx="50%"
          cy="50%"
          innerRadius={80}
          outerRadius={110}
          fill={chartColors.activeInfected}
        />
      </PieChart>
    </ResponsiveContainer>
  );
};

export default CustomPieChart;
