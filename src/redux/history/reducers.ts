import { Store } from "@redux/store";
import { ISetHistoryAction } from "@redux/history/actions";


export const setHistoryReducer = (store: Store, { payload: { history } }: ISetHistoryAction): Store => {
  store.history = history;
  return {
    ...store,
    history
  };
};
