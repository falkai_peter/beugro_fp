import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import { FC } from "react";
import MomentUtils from "@date-io/moment";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import moment from "moment";

type Props = {
  selectedDate: Date;
  label: string,
  onDateChange: (
    date: MaterialUiPickersDate,
    value?: string | null | undefined
  ) => void;
};

const CustomDatePicker: FC<Props> = props => {
  return (
    <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils}>
      <KeyboardDatePicker
        disableToolbar
        variant="inline"
        label={props.label}
        format="yyyy/MM/DD"
        margin="normal"
        value={props.selectedDate}
        onChange={props.onDateChange}
      />
    </MuiPickersUtilsProvider>
  );
};

export default CustomDatePicker;
