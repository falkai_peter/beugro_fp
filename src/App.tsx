import CustomAreaChart from "@components/AreaChart/AreaChart";
import CustomDatePicker from "@components/DatePicker/DatePicker";
import CustomLineChart from "@components/LineChart/LineChart";
import CustomPieChart from "@components/PieChart/PieChart";
import { CircularProgress, Grid, Tab, Tabs } from "@material-ui/core";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import { setHistory } from "@redux/history/actions";
import { getInterval } from "@redux/history/selectors";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Route, useHistory } from "react-router";
import useStyles from "./styles"

function App() {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();

  useEffect(() => {
    dispatch(setHistory());
  }, [dispatch]);
  
  const [currentTabIndex, setCurrentTabIndex] = useState(0);
  const [timeInterval, setTimeInterval] = useState({
    from: moment().subtract(1, 'days'),
    to: moment(),
  });
  
  const interv = useSelector(getInterval(timeInterval.from, timeInterval.to));

  
  
  const handleTabIndexChange = (event: React.ChangeEvent<{}>, val: number) => {
    setCurrentTabIndex(val);
    history.push(val.toString());
  };
  
  const handleFromChange = (date: MaterialUiPickersDate) => {
    setTimeInterval({
      ...timeInterval,
      from: moment(date),
    });
  };

  const handleToChange = (date: MaterialUiPickersDate) => {
    setTimeInterval({
      ...timeInterval,
      to: moment(date),
    });
  };

  return (
    <div className={classes.root}>
      <Grid container justify="center" alignItems="center">
        <CustomDatePicker
          label="From"
          onDateChange={handleFromChange}
          selectedDate={timeInterval.from.toDate()}
        ></CustomDatePicker>
        <CustomDatePicker
          label="To"
          onDateChange={handleToChange}
          selectedDate={timeInterval.to.toDate()}
        ></CustomDatePicker>
      </Grid>
      <Tabs onChange={handleTabIndexChange} value={currentTabIndex} centered>
        <Tab label={"Line"}></Tab>
        <Tab label={"Pie"}></Tab>
        <Tab label={"Area"}></Tab>
      </Tabs>
      <Route
        path="/0"
        render={() => <CustomLineChart data={interv} height={700} />}
      />
      <Route
        path="/1"
        render={() => <CustomPieChart data={interv} height={700} />}
      />
      <Route
        path="/2"
        render={() => <CustomAreaChart data={interv} height={700} />}
      />
      {!interv.length && <CircularProgress className={classes.spinner} />}
    </div>
  );
}

export default App;
