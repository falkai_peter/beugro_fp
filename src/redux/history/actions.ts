import history from "@fetcher/history";
import ICovidData from "src/types/ICovidData";
import { Action } from "redux";

export interface ISetHistoryAction extends Action<"SET_HISTORY"> {
  payload: {
    history: ICovidData[];
  };
}

export const setHistory = async (): Promise<ISetHistoryAction> => {
  try {
    return {
      type: "SET_HISTORY",
      payload: {
        history: await history(),
      },
    };
  } catch (error) {
    return {
      type: "SET_HISTORY",
      payload: {
        history: [],
      },
    };
  }
};
