export default interface ICovidData {
  infected: number,
  recovered: number,
  activeInfected: number
  lastUpdatedAtApify:string
}