import ICovidData from "src/types/ICovidData";
import React, { FC } from "react";
import {
  AreaChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Area,
  ResponsiveContainer,
} from "recharts";

import chartColors from "@components/chartColors"

type Props = {
  height: number;
  data: ICovidData[];
};

const CustomAreaChart: FC<Props> = props => {
  return (
    <ResponsiveContainer width="100%" height={props.height}>
      <AreaChart data={props.data}>
        <defs>
          <linearGradient id="colorInfected" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor={chartColors.infected} stopOpacity={0.8} />
            <stop offset="95%" stopColor={chartColors.infected} stopOpacity={0} />
          </linearGradient>
          <linearGradient id="colorRecovered" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor={chartColors.recovered} stopOpacity={0.8} />
            <stop offset="95%" stopColor={chartColors.recovered} stopOpacity={0} />
          </linearGradient>
          <linearGradient id="colorActiveInfected" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor={chartColors.activeInfected} stopOpacity={0.8} />
            <stop offset="95%" stopColor={chartColors.activeInfected} stopOpacity={0} />
          </linearGradient>
        </defs>
        <XAxis dataKey="name" />
        <YAxis />
        <CartesianGrid strokeDasharray="3 3" />
        <Tooltip />
        <Area
          type="monotone"
          dataKey="infected"
          stroke="#8884d8"
          fillOpacity={1}
          fill="url(#colorInfected)"
        />
        <Area
          type="monotone"
          dataKey="recovered"
          stroke="#82ca9d"
          fillOpacity={1}
          fill="url(#colorRecovered)"
        />
        <Area
          type="monotone"
          dataKey="activeInfected"
          stroke="#82ca9d"
          fillOpacity={1}
          fill="url(#colorActiveInfected)"
        />
      </AreaChart>
    </ResponsiveContainer>
  );
};

export default CustomAreaChart;
