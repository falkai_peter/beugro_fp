import ICovidData from "src/types/ICovidData";
import React, { FC } from "react";
import {
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  Line,
  LineChart,
  ResponsiveContainer,
} from "recharts";
import chartColors from "@components/chartColors";

type Props = {
  height: number;
  data: ICovidData[];
};

const CustomLineChart: FC<Props> = props => {
  return (
    <ResponsiveContainer height={props.height} width="100%">
      <LineChart data={props.data}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="lastUpdatedAtApify" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line
          type="monotone"
          dataKey="infected"
          stroke={chartColors.infected}
        />
        <Line
          type="monotone"
          dataKey="recovered"
          stroke={chartColors.recovered}
        />
        <Line
          type="monotone"
          dataKey="activeInfected"
          stroke={chartColors.activeInfected}
        />
      </LineChart>
    </ResponsiveContainer>
  );
};

export default CustomLineChart;
