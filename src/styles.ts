import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
  spinner: {
    position: "fixed",
    top: "50%",
    left: "50%"
  }
}));

export default useStyles;
