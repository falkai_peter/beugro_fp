const chartColors = {
  infected: "#ffff00",
  recovered: "#00ff00",
  activeInfected: "#ff0000",
}

export default chartColors