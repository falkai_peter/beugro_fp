import { Store } from "@redux/store";
import { Moment } from "moment";

export const getHistory = () => (store:Store) => store.history;

export const getInterval = (from: Moment, to: Moment) => (store:Store) => {
  const fUTCdate = from.utc().toISOString().split('T')[0]
  const tUTCdate = to.utc().toISOString().split('T')[0]
  const startIndex = store.history.findIndex(d => d.lastUpdatedAtApify.split('T')[0] === fUTCdate)
  const endIndex = store.history.findIndex(d => d.lastUpdatedAtApify.split('T')[0] === tUTCdate)
  return store.history.slice(startIndex, endIndex);
}