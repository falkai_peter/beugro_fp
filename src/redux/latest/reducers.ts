import { Store } from "@redux/store";
import { ISetLatestAction } from "@redux/latest/actions";


export const setLatestReducer = (store: Store, { payload: { latest } }: ISetLatestAction): Store => {
  store.latest = latest;
  return {
    ...store,
    latest
  };
};
