import { Store } from "@redux/store";

export const getLatest = () => (store:Store) => store.latest;