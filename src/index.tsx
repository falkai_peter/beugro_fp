import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { store } from "@redux/store";
import { MemoryRouter } from "react-router";


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <MemoryRouter initialEntries={["/0"]}>
        <App />
      </MemoryRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
reportWebVitals();
