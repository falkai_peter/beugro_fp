import { Action } from "redux";
import { initStore, Store } from "@redux/store";
import { setLatestReducer } from "@redux/latest/reducers";
import { ISetLatestAction } from "@redux/latest/actions";
import { ISetHistoryAction } from "@redux/history/actions";
import { setHistoryReducer } from "@redux/history/reducers";

const reducer = (store: Store = initStore, action: Action) => {
  switch (action.type) {
    case "SET_LATEST":
      return setLatestReducer(store, action as ISetLatestAction);
    case "SET_HISTORY":
      return setHistoryReducer(store, action as ISetHistoryAction);
    default:
      return store;
  }
};

export default reducer;
