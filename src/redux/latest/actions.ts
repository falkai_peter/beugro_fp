import latest from "@fetcher/latest";
import ICovidData from "src/types/ICovidData";
import { Action } from "redux";

export interface ISetLatestAction extends Action<"SET_LATEST"> {
  payload: {
    latest: ICovidData
  };
}

export const setLatest = async (): Promise<ISetLatestAction> => {
  try {
    return {type: "SET_LATEST",
    payload: {
      latest: (await latest())
    }}
    
  } catch (error) {
    return {type: "SET_LATEST",
    payload: {
      latest: {activeInfected: 0, infected: 0, recovered: 0, lastUpdatedAtApify: ''}
    }}
    
  }
};
